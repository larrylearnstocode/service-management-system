const express = require("express");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");
const mysql = require("mysql2");
const multer = require("multer");
const saltRounds = 10;
const secret = "shh";
const upload = multer({ dest: 'uploaded-files' });
const app = express();
const fs = require("fs");
let pool = null;

const port = 3000;


const payload = {
    "name": ""
};

function auth(req, res, next) {
    console.log("in auth middleware");
    //the bearer token is in the header
    const authorizationHeader = req.headers.authorization;

    if (authorizationHeader) {

        //split the word "bearer" and token is extaactable
        const splittedAuth = authorizationHeader.split(" ");

        console.log(authorizationHeader);

        if (splittedAuth[0] === "Bearer") {
            const token = splittedAuth[1];
            jwt.verify(token, secret, function (err, decoded) {

                if (err) {
                    res.status(401);
                    return res.json({
                        success: false
                    });
                }
                req.tokenPayload = decoded; //// for middle ware user
                console.log("line 42", decoded);
                next();  // for middle ware user
            });
        } else {
            res.status(401);
            return res.json({
                success: false
            });
        }

    } else {
        res.json({
            "your header might be missing": true
        })
    }
}


function gentJWT(payload) {
    // const options = {
    //     expiresIn: "3600s",
    // };
    return jwt.sign(payload, secret);
}

function getRefreshJWT(payload) {
    return jwt.sign(payload, secret);
}

function formatTime(date) {
    let month = (date.getMonth() + 1).toString();
    let day = date.getDate().toString();
    let year = date.getFullYear();
    if (month.length < 2) {
        month = '0' + month;
    }
    if (day.length < 2) {
        day = '0' + day;
    }
    return [year, month, day].join('-');
}

// async function getService(req, res, next) {
//     let targetService = parseInt(req.params.serviceId);

//     const [result] = await pool.execute(`select * from service where id=?`, [targetService]);

//     console.log(result);

//     if (result.length !== 0) {
//         req.getService = result[0];
//         next();
//     } else {
//         res.json({
//             "error in getService": 404
//         })
//     }
// }

app.get("/", (req, res) => {
    res.send("hello world")
})

app.use(express.json());
app.use("/uploaded-files", express.static('uploaded-files'));


//////////////////////////////////////////////////////////
///////////////      Members register        ////////////////
//////////////////////////////////////////////////////////

app.post("/register", async (req, res) => {
    const username = req.body.username;
    const password = req.body.password;
    const joinDate = new Date();
    // const dateDate = joinDate.toString().split('T')[0];
    const realDate = formatTime(joinDate);


    try {
        const [result] = await pool.execute(`select username from member where username=?`, [username]);
        console.log(result);
        if (result.length === 0) {
            console.log("username is valid");

            bcrypt.hash(password, saltRounds, async function (err, hash) {
                if (err) {
                    return res.json({
                        success: false
                    })
                }

                try {

                    const [result2] = await pool.execute(`insert into member (username, password, joinDate) values (?,?,?)`, [username, hash, realDate]);
                    console.log(result);
                    console.log([result2]);
                    // const [result, fields] = await pool.query(`insert into member (id, username, password, joinDate) values (id, '${username}', '${hash}', '${realDate}')`);

                    let token = gentJWT({ // payload
                        username: username,
                    });
                    let refreshToken = getRefreshJWT({
                        username: username,
                    });

                    res.json({
                        token: token,
                        refreshToken: refreshToken,
                    });


                } catch (error) {
                    console.log(error);
                    res.json({
                        success: false
                    })
                }
            })

        } else {
            console.log("username is used");
            res.json({
                message: "username is taken"
            })
        }

    } catch (error) {
        res.json({
            success: false
        })
    }

})


//////////////////////////////////////////////////////////
///////////////      Members login        ////////////////
//////////////////////////////////////////////////////////

app.post("/login", async (req, res) => {
    const username = req.body.username;
    const password = req.body.password;

    try {

        const [result] = await pool.execute(`select username, password from member where username =?`, [username]);

        console.log(result);
        console.log(result[0].username);
        let usernameInDatabase = result[0].username;
        console.log(result[0].password);
        let passwordInDatabase = result[0].password;

        bcrypt.compare(password, passwordInDatabase, function (err, result) {
            if (err) {
                return res.json({
                    success: false
                })
            }
            if (result) {
                let token = gentJWT({
                    username: username
                });
                let refreshToken = getRefreshJWT({
                    username: username
                });
                res.json({
                    token: token,
                    refreshToken: refreshToken
                });
            } else {
                res.json({
                    success: false
                })
            }
        })


    } catch (error) {

        res.json({
            error: "user not found"
        })
    }

})

// app.post("/members/logout", auth, (req, res) => {
//     req.headers.token = null;
//     res.json({
//         success: true
//     })
// })

//////////////////////////////////////////////////////////////////////
///////////////      Members can create a service     ////////////////
//////////////////////////////////////////////////////////////////////


app.post("/members/service", auth, async (req, res) => {
    let serviceOwner = req.tokenPayload.username;
    let name = req.body.name;
    let description = req.body.description;
    let likeCount = 0;


    try {
        console.log("hello");
        const [result] = await pool.execute(`insert into service(owner, name, description, likeCount, availability, status) values (?,?,?,?,?,?)`, [serviceOwner, name, description, likeCount, true, true]);
        console.log(result);

        const [result2] = await pool.execute(`select * from service where owner=?`, [serviceOwner]);
        console.log("result2", result2);

        let y = result2.length
        let newService = result2[y - 1];
        console.log(newService);
        res.json(newService);

    } catch (error) {
        res.json({
            success: false
        })
    }
})


//////////////////////////////////////////////////////////
/////   Users can search the name of available        ////
//////////////////////////////////////////////////////////
/////   localhost:3000/service/?name={e.g.coding}    ///// 
//////////////////////////////////////////////////////////

app.get("/service", async (req, res) => {

    console.log(req.query.name);
    let serviceName = req.query.name;
    console.log("outside the wall")
    if (serviceName) {
        try {
            console.log("serviceName")
            //setting query can save more code on here plus increase efficiency
            const [result] = await pool.execute(`select * from service where name like ? and status=? and availability=?`, [`%${serviceName}%`, true, true]);
            //////////////// maybe .....??   LIKE '%keyword%' /////////
            console.log(result);

            if (result.length !== 0) {
                res.json(result)
            } else {
                res.json({
                    thereIsNoSuchService: true
                })
            }
        } catch (error) {
            console.log(error);
            res.json({
                success: false
            })
        }
    } else {
        const [result2] = await pool.execute(`select * from service where status=? and availability=?`, [true, true]);
        res.json(result2)
    }

})


///////////////////////////////////////////////////////////////////////////
//////   getting service details, join two tables together:    ////////////
//////   service + photo relating to the servie                ////////////
///////////////////////////////////////////////////////////////////////////

app.get("/service/:serviceId/details", async (req, res) => {
    let serviceId = req.params.serviceId;
    console.log(serviceId);

    try {

        const [result] = await pool.execute(`
            select * from service 
            left join picture 
            on picture.service_id = service.id 
            left join comment 
            on comment.service_id = service.id 
            where service.id=?`, [serviceId]);

        console.log("result of joined tables", result);

        function cleaning(accumulator, current) {

            if (accumulator[current.id]) {
                //maybe i can use array.some()

                if (!accumulator[current.id].picture.find((pic) => {
                    return pic.picture_id === current.picture_id
                }) && current.picture_id !== null) {
                    console.log("line 341", accumulator);

                    accumulator[current.id].picture.push({
                        "picture_id": current.picture_id,
                        "link": "localhost:3000/" + current.link,
                        "filename": current.filename
                    });
                }
                ////// i added the second part to the condition
                if (!accumulator[current.id].comment.find((cmt) => {
                    return cmt.comment_id === current.comment_id
                }) && current.comment_id !== null) {
                    console.log("line 354", accumulator);
                    accumulator[current.id].comment.push({
                        "comment_id": current.comment_id,
                        "content": current.content,
                        "author": current.author,
                        "created_on": formatTime(current.created_on)
                    })
                }

            } else {

                accumulator[current.id] = {};
                accumulator[current.id].owner = current.owner;
                accumulator[current.id].name = current.name;
                accumulator[current.id].description = current.description;
                accumulator[current.id].likeCount = current.likeCount;
                accumulator[current.id].availability = current.availability;
                accumulator[current.id].likeCount = current.likeCount;
                accumulator[current.id].status = current.status;
                accumulator[current.id].picture = [];
                accumulator[current.id].comment = [];

                if (current.picture_id !== null) {

                    accumulator[current.id].picture.push({
                        "picture_id": current.picture_id,
                        "link": "localhost:3000/" + current.link,
                        "filename": current.filename
                    });
                }

                if (current.comment_id !== null) {

                    accumulator[current.id].comment.push({
                        "comment_id": current.comment_id,
                        "content": current.content,
                        "author": current.author,
                        "created_on": formatTime(current.created_on)
                    })
                }
            }

            return accumulator;
        }

        let cleanedResult = result.reduce(cleaning, {});

        if (result.length !== 0) {
            res.json(cleanedResult)
        } else {
            res.json({
                message: "noSuchService"
            })
        }

    } catch (error) {
        console.log(error);

        res.json({
            success: false
        })
    }
})


////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
///////    service owner can see the name of their customers      //////////
////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////


app.get("/service/my-booking-record", auth, async (req, res) => {

    console.log(req.tokenPayload.username);

    try {

        const [result] = await pool.execute(`select id from service where owner=?`, [req.tokenPayload.username]);
        console.log(result);

        if (result.length !== 0) {


            const [result] = await pool.execute(`
            select * from bookingRecord 
            left join service 
            on bookingRecord.service_id = service.id 
            where service.owner=?`, [req.tokenPayload.username]);


            if (result.length === 0) {
                res.json({
                    message: "your service(s) has no taker yet"
                })
            } else {

                res.json(result);
            }

        } else {
            res.json({
                message: "you are not hosting any service"
            })
        }

    } catch (error) {
        console.log(error);
        res.json({ error })
    }
})


//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
///////////   service owner can edit service info  ///////////////////////
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

app.patch("/members/service/:serviceId", auth, async (req, res) => {

    // if (req.tokenPayload.username === target.owner) {
    // console.log(req.tokenPayload.username);
    let name = req.body.name;
    let description = req.body.description;


    try {

        const [result] = await pool.execute(`select id from service where owner=? and id=?`, [req.tokenPayload.username, req.params.serviceId]);
        console.log("line 376", result);
        if (result.length === 0) {
            res.json({
                error: true,
                "message": "not the owner or no such service"
            })
            return;
        }

        let target = result[0];


        let updateQuery = [];
        let preparedStatementParams = [];
        if (name) {
            updateQuery.push("name=?");
            preparedStatementParams.push(name);
        }
        if (description) {
            updateQuery.push("description=?");
            preparedStatementParams.push(description);
        }

        let query = updateQuery.join(","); //so it's not an array anymore, just a string 
        console.log(`update service set ${query} where id=?`);
        // console.log(`update service set name=? description=? where id=?`);

        preparedStatementParams.push(target.id);


        const [result2] = await pool.execute(`update service set ${query} where id=?`, preparedStatementParams);
        console.log(result2);
        const [result3] = await pool.execute(`select * from service where id=?`, [target.id]);
        console.log(result3[0]);

        res.json({
            result: true
        });


    } catch (error) {
        console.log(error);
        res.json({
            result: false
        });
    }
})

///////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////
/////////////      post pictures for the service    ///////////////
///////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////

app.post("/members/:serviceId/servicePictures", auth, upload.single("beautifulPhoto"), async (req, res) => {

    // if (req.tokenPayload.username === target.serviceOwner) {

    try {
        const [result] = await pool.execute(`select 1 from service where owner=? and id=?`, [req.tokenPayload.username, req.params.serviceId])

        if (result.length === 0) {
            res.json({
                message: "you have no power to post pictures for others' service"
            })
            return;
        }

        const [result2] = await pool.execute(`insert into picture (filename, link, service_id) values (?,?,?)`, [req.file.filename, req.file.path, req.params.serviceId]);


        /// if you want to see the picture just uploaded to a service:
        // const [result3] = await pool.execute(`select * from picture where service_id=?`, [req.params.serviceId])
        // let y= result3.length;
        // let newPicture = result3[y-1];
        // console.log("line 536",newPicture);
        // console.log(result3);

        res.json({
            message: "addPictureSuccess"
        })

    } catch (error) {
        console.log(error);
        res.json({
            message: "caught an error"
        })
    }

})


app.patch("/undo-delete/:serviceId", async (req, res) => {

    try {
        const [result] = await pool.execute(`update service set status = ? where id=?`, [true, req.params.serviceId]);
        // const [result2] = await pool.execute(`select * from service where id=?`, [req.params.serviceId]);
        console.log();
        res.json({
            message: "the service is back"
        })
    } catch (error) {
        console.log(error);
        res.json({
            message: "error occured"
        })
    }
})

//\/////////\///////////\////////////\\\\\\\\\\\\

//\//////   whole service is deleted  ///////////

//\/////////\///////////\////////////\\\\\\\\\\\\

app.delete("/members/service/:serviceId", auth, async (req, res) => {


    try {
        const [result] = await pool.execute(`select 1 from service where id=? and availability=? and status=? and owner=?`, [req.params.serviceId, true, true, req.tokenPayload.username]);
        console.log("642", result);
        if (result.length === 0) {
            res.json({
                error: true,
                message: "no such service found or you are not the serivce owner"
            })
            return;
        }

        try {

            const [result] = await pool.execute(`update service set status=? where owner=? and id=?`, [false, req.tokenPayload.username, req.params.serviceId])
            const [result2] = await pool.execute(`select status from service where id=?`, [req.params.serviceId]);

            if (result2[0].status === 0) {
                res.json({
                    deleteService: true
                })
            } else {
                res.json({
                    "error": "only owner can delete thier service"
                })
            }

            //// to remove the files uploaded to the static folder 
            try {

                const [result3] = await pool.execute(`select link from picture where service_id=?`, [req.params.serviceId]);

                if (result3.length === 0) {
                    return
                } else {
                    const [result4] = await pool.execute(`delete from picture where service_id=?`, [req.params.serviceId]);
                }


                for (let i = 0; i < result3.length; i++) {

                    const pathToFile = result3[i].link;
                    console.log(pathToFile);

                    try {
                        fs.unlinkSync(pathToFile)
                        console.log("Successfully deleted the file.")
                    } catch (err) {
                        console.log(err);
                        throw err
                    }
                }

            } catch (error) {
                res.json({
                    success: false
                })
            }


        } catch (error) {
            res.json({
                success: false
            })
        }


    } catch (error) {
        res.json({
            "message": "unexpected error"
        })
    }

})

///////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////
////////////////   updating like count     ////////////////
///////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////


app.patch("/members/service/:serviceId/like", auth, async (req, res) => {

    try {
        const [result] = await pool.execute(`update service set likeCount = likeCount + 1 where id=?`, [req.params.serviceId])
        console.log(result);
        if (result.affectedRows === 0) {
            res.json({
                message: "service not exist"
            })
            return;
        }

        res.json({
            updateLikeCount: true
        })

    } catch (error) {
        res.json({
            error: true
        })
    }
})

//////////////////////////////////////////////////
//////////////////////////////////////////////////
/////  members can post comments to service   ////
//////////////////////////////////////////////////
//////////////////////////////////////////////////

app.post("/members/service/:serviceId/comment", auth, async (req, res) => {


    let commentTime = formatTime(new Date());

    try {

        const [result] = await pool.execute(`select 1 from service where id=?`, [req.params.serviceId]);
        console.log(result);
        if (result.length === 0) {
            res.json({
                error: true,
                "message": "no such service found"
            })
            return;
        }

        const [result2] = await pool.execute(`insert into comment (content, author, created_on, service_id) values (?,?,?,?)`, [req.body.content, req.tokenPayload.username, commentTime, req.params.serviceId]);
        console.log(result2);

        res.json({
            message: true
        })

    } catch (error) {
        console.log(error);
        res.json({
            success: false
        })
    }
})


////////////////////////////////////////////////////////////////////
//////// members book services and change availability to false/////
////////////////////////////////////////////////////////////////////


app.patch("/members/service/:serviceId/booking", auth, async (req, res) => {

    try {

        const [result] = await pool.execute(`select 1 from service where id=? and availability=? and status=?`, [req.params.serviceId, true, true]);
        console.log("628", result);
        if (result.length === 0) {
            res.json({
                error: true,
                message: "no such service found"
            })
            return;
        }
        //and availability=? and status=? and id =?
        //, true, true, req.params.serviceId
        const [result2] = await pool.execute(`select 1 from service where owner=? and id=? `, [req.tokenPayload.username, req.params.serviceId]);
        console.log("line 638", result2);
        if (result2.length === 0) {
            let bookDate = formatTime(new Date());

            const [result4] = await pool.execute(`update service set availability=? where id=?`, [false, req.params.serviceId])
            const [result3] = await pool.execute(`insert into bookingRecord (booked_by, service_id, booking_made_on) values (?,?,?)`, [req.tokenPayload.username, req.params.serviceId, bookDate])

            res.json({
                booking: "success"
            })

        } else {
            res.json({
                "message": "service is not avaiable or you cant book your own service"
            })
        }

    } catch (error) {
        console.log(error);
        res.json({
            success: false
        })
    }

})


////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
////      delete someone's booking and free up the service      ////////////
////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////

app.delete("/members/service/:serviceId/booking", auth, async (req, res) => {


    try {
        const [result] = await pool.execute(`select 1 from service where id=? and availability =? and status=?`, [req.params.serviceId, false, true]);
        console.log(result);
        if (result.length === 0) {
            res.json({
                error: true,
                message: "no such service found"
            })
            return;
        }

        const [result2] = await pool.execute(`select 1 from service where id=? and owner=?`, [req.params.serviceId, req.tokenPayload.username]);
        if (result2.length === 0) {
            res.json({
                message: "only owner can delete booking"
            })
            return;
        }

        const [result3] = await pool.execute(`update service set availability=?`, [true]);

        res.json({
            success: true
        })

    } catch (error) {
        console.log(error)
        res.json({
            "message": "somethings wrong"
        })
    }

})

app.get("/*/", (req, res) => {
    res.json({
        error: "404 not found"
    })
})


app.listen(3000, () => {
    console.log("listening on port 3000!");
    pool = mysql.createPool({
        host: "localhost",
        port: 3306,
        user: "root",
        password: "root",
        database: "serviceBookingApp"
    }).promise();
})